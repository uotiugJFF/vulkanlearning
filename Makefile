# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2024/05/06 13:27:20 by gbrunet           #+#    #+#              #
#    Updated: 2024/05/06 13:29:48 by gbrunet          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CFLAGS = -std=c++17 -O2

LDFLAGS = -lglfw -lvulkan -ldl -lpthread -lX11 -lXxf86vm -lXrandr -lXi

VulkanLearning: main.cpp
	c++ $(CFLAGS) -o VulkanLearning main.cpp $(LDFLAGS)

.PHONY: test clean

test: VulkanLearning
	./VulkanLearning

clean:
	rm -f VulkanLearning
